package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class EditController {
    Stage dialogStage;
    Task task;

    @FXML Button add=new Button();
    @FXML TextField title = new TextField();
    @FXML TextField description= new TextField();
    @FXML ObservableList<String> priorityList= FXCollections.observableArrayList("Low","Medium","High");
    @FXML ComboBox priority = new ComboBox(priorityList);
    @FXML DatePicker date = new DatePicker();

    @FXML
    private void initialize() {
        priority.getItems().addAll("Low","Medium","High");
        }

    private void show() {
        if (task != null) {
            // Fill the labels with info from the person object.
           title.setText(task.title);
           priority.setValue(task.priority);
           date.setValue(task.expDate);
           description.setText(task.description);



        }
    }

    @FXML
    void edit(ActionEvent e) {
        task.setTitle(title.getText());
        task.setDescription(description.getText());
        task.setPriority((String) priority.getValue());
        task.setExpDate(date.getValue());



        dialogStage.close();
    }



    public void setDialogStage(Stage dialogStage) {
        this.dialogStage=dialogStage;
    }

    public void setTask(Task task) {
        this.task=task;
        show();
    }
}
