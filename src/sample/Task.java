package sample;

import java.io.Serializable;
import java.time.LocalDate;

public class Task implements Serializable {
    String title,priority,description;
    LocalDate expDate;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public LocalDate getExpDate() {
        return expDate;
    }

    public void setExpDate(LocalDate expDate) {
        this.expDate = expDate;
    }

    public Task() {
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task(String title, String priority, String description, LocalDate expDate) {
        this.title = title;
        this.priority = priority;
        this.description = description;
        this.expDate = expDate;
    }
}
