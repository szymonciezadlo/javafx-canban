package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Controller2 {

    @FXML Button add=new Button();
    @FXML TextField title = new TextField();
    @FXML TextField description = new TextField();
    @FXML ComboBox<String> priority = new ComboBox();
    @FXML DatePicker date = new DatePicker();
    Task task;


    private Controller controller;
    private Stage dialogStage;

    @FXML void initialize(){
        priority.getItems().addAll("Low","Medium","High");
    }


    @FXML
    void addTask(ActionEvent e)
    {

        /*System.out.println("AS");
        System.out.println(title.getText());
        System.out.println(priority.getValue());
        System.out.println(date.getValue());
        LocalDate date1=date.getValue();*/

        //task=new Task(title.getText(), (String) priority.getValue(),date.getValue());

        task.setTitle(title.getText());
        task.setPriority((String) priority.getValue());
        task.setExpDate(date.getValue());
        task.setDescription(description.getText());

        dialogStage.close();


    }


    void XD(String task){
        //toDoList.getItems().add("task");
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage=dialogStage;
    }

    public void setTask(Task newTask) {
        this.task=newTask;
    }
}
