package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class Controller extends ListView<Task>{
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-d");
    @FXML Button addButton = new Button();
    @FXML ListView<Task> toDoList=new ListView();
    @FXML ListView<Task> inProgressList=new ListView();
    @FXML ListView<Task> doneList=new ListView();
    Stage primaryStage;
    //@FXML Tooltip toDoTooltip = new Tooltip();
    ListView<Task> whichList=new ListView();

    class TaskCell extends ListCell<Task>{
        @Override
        protected void updateItem(Task item, boolean empty) {
            super.updateItem(item, empty);
            if(!empty) {
                setTooltip(new Tooltip(item.description));
                setText(item.title);
                if(item.priority.equals("Low"))setStyle("-fx-background-color: darkseagreen;");
                if(item.priority.equals("Medium"))setStyle("-fx-background-color: lightblue;");
                if(item.priority.equals("High"))setStyle("-fx-background-color: yellow;");
            }
            else{
                setText("");
                setStyle("-fx-background-color: white;");
            }


        }
    }


    FileChooser fileChooser = new FileChooser();
    @FXML void initialize(){
        toDoList.setCellFactory(e -> new TaskCell());
        inProgressList.setCellFactory(e -> new TaskCell());
        doneList.setCellFactory(e -> new TaskCell());
        Task task = new Task("A","Low","AAAAAAAAAAAAAAAAAAA",LocalDate.now());
        Task task1 = new Task("B","Medium","BBBBBBBBBBBBBBBBBB",LocalDate.now());
        Task task2 = new Task("C","High","CCCCCCCCCCCCCCCCCCCC",LocalDate.now());
        toDoList.getItems().add(task);
        toDoList.getItems().add(task1);
        toDoList.getItems().add(task2);
        toDoList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);


        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("BIN Files", "*.bin")
        );

        //toDoList.setItems(taskList);
    }




    @FXML
    void addToList(ActionEvent e)throws IOException{


        // Load the fxml file and create a new stage for the popup dialog.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controller.class.getResource("addToListWindow.fxml"));
        AnchorPane page = (AnchorPane) loader.load();

        // Create the dialog Stage.
        Stage dialogStage = new Stage();

        dialogStage.setTitle("Edit Person");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        // Set the person into the controller.
        Controller2 controller2 = loader.getController();
        controller2.setDialogStage(dialogStage);
        Task newTask= new Task();
        controller2.setTask(newTask);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        //taskList.add(newTask);
        if(newTask.title!=null && (String) newTask.priority!=null && newTask.expDate!=null)toDoList.getItems().add(newTask);

        //toDoList.getItems().add(newTask.title);
        //toDoTooltip.


    }


    @FXML
    void edit(ActionEvent e)throws IOException {
        // Load the fxml file and create a new stage for the popup dialog.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controller.class.getResource("Edit.fxml"));
        AnchorPane page = (AnchorPane) loader.load();

        // Create the dialog Stage.
        Stage dialogStage = new Stage();

        dialogStage.setTitle("Edit Person");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        // Set the person into the controller.
        EditController controller = loader.getController();
        controller.setDialogStage(dialogStage);
        Task newTask = new Task();
        //ListView<Task> whichList = whichListIs();


        newTask = whichList.getSelectionModel().getSelectedItem();
        controller.setTask(newTask);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();
        del(whichList);
        whichList.getItems().add(newTask);
    }


    /*ListView<Task> whichListIs() throws NullPointerException {
        if (toDoList.getSelectionModel().getSelectedItem() != null) return toDoList;
        if (inProgressList.getSelectionModel().getSelectedItem() != null) return inProgressList;
        if (doneList.getSelectionModel().getSelectedItem() != null) return doneList;
        throw new NullPointerException();
        }*/

        @FXML void toDoClick(MouseEvent e){whichList=toDoList; }
        @FXML void inProgressClick(MouseEvent e){whichList=inProgressList; }
        @FXML void doneClick(MouseEvent e){whichList=doneList; }


        @FXML void moveToRight (ActionEvent e)throws IOException {
            if (whichList==toDoList) {
                inProgressList.getItems().add(toDoList.getSelectionModel().getSelectedItem());
                del(toDoList);
            }
            if (whichList==inProgressList) {
                doneList.getItems().add(inProgressList.getSelectionModel().getSelectedItem());
                del(inProgressList);
            }

        }

        @FXML
        void delete (ActionEvent e){
            del(whichList);
        }

        void del (ListView < Task > A) {
            A.getItems().remove(A.getSelectionModel().getSelectedIndex());
        }

        @FXML void endProgram (ActionEvent e){
            primaryStage.close();
        }

        public void setDialogStage (Stage dialogStage){
            this.primaryStage = dialogStage;
        }

        @FXML void about(ActionEvent e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information about Author");
            alert.setHeaderText(null);
            alert.setContentText("Szymon Ciężadło IS grLab:2");

            alert.showAndWait();
        }



        @FXML void save(ActionEvent e) throws IOException {
            File selectedFile = fileChooser.showOpenDialog(primaryStage);
            try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(selectedFile))) {
                    output.writeObject(new ArrayList<>(toDoList.getItems()));
                    output.writeObject(new ArrayList<>(inProgressList.getItems()));
                    output.writeObject(new ArrayList<>(doneList.getItems()));
            }
        }

        @FXML void read(ActionEvent e)throws IOException,ClassNotFoundException{
            clearLists();
            File selectedFile = fileChooser.showOpenDialog(primaryStage);
            try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(selectedFile))) {
                toDoList.getItems().addAll((ArrayList<Task>) input.readObject());
                inProgressList.getItems().addAll((ArrayList<Task>) input.readObject());
                doneList.getItems().addAll((ArrayList<Task>) input.readObject());
            }
            catch (EOFException eof){}
        }

        @FXML void export(ActionEvent e){
            try(BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("export.csv"), "UTF-8"))) {
            exportLoop(bw,toDoList);
            exportLoop(bw,inProgressList);
            exportLoop(bw,doneList);
            }
            catch (IOException ev){}
        }
        void exportLoop(BufferedWriter bw,ListView<Task> list){
            String CSV_SEPARATOR=";";
            try
            {

               for (Task task : list.getItems())
                {
                    StringBuffer oneLine = new StringBuffer();
                    oneLine.append(task.getTitle());
                    oneLine.append(CSV_SEPARATOR);
                    oneLine.append(task.getPriority());
                    oneLine.append(CSV_SEPARATOR);
                    oneLine.append(task.getDescription());
                    oneLine.append(CSV_SEPARATOR);
                    oneLine.append(task.getExpDate().format(formatter));
                    bw.write(oneLine.toString());
                    bw.newLine();
                }
               bw.newLine();
               bw.flush();

            }
            catch (IOException ev){}
        }

        @FXML void impor(ActionEvent e){

            try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("export.csv"), "UTF-8"))){
                clearLists();

                String line;
                while (!(line = br.readLine()).equals("")) {
                    String[] values = line.split(";");
                    LocalDate dat=LocalDate.parse(values[3],formatter);
                    toDoList.getItems().add(new Task(values[0],values[1],values[2],dat));

                }
                while (!(line = br.readLine()).equals("")) {
                    String[] values = line.split(";");
                    //String date[]=values[3].split("-");
                    LocalDate dat=LocalDate.parse(values[3],formatter);
                    inProgressList.getItems().add(new Task(values[0],values[1],values[2],dat));
                }
                while (!(line = br.readLine()).equals("")) {
                    String[] values = line.split(";");
                    //String date[]=values[3].split("-");
                    LocalDate dat=LocalDate.parse(values[3],formatter);
                    doneList.getItems().add(new Task(values[0],values[1],values[2],dat));
                }

            }
            catch (ArrayIndexOutOfBoundsException ev){
                System.err.println("Something wrong with data");
                clearLists();
            }
            catch (IOException ev){}

        }

        void clearLists(){
            toDoList.getItems().clear();
            inProgressList.getItems().clear();
            doneList.getItems().clear();
        }




    }

